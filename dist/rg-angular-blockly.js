'use strict';

angular.module("rg-angular-blockly", [])
    .provider("ngBlockly", function () {
        var ngBlocklyProviderFactory = {};

        this.options = {
            path: "./assets/blockly/media/",
            trashcan: true,
            toolbox: []
        };

        this.getOptions = function(){
            return this.options;
        };

        this.$get = function () {
            return this;
        };

        this.setOptions = function (options) {
            this.options = options;
        };
    })
    .service('rgBlockly', ['$timeout', function($timeout){
    	var me = this;
        this.holdoffChanges = false;

        this.setWorkspace = function (workspace) {
            if (Blockly.getMainWorkspace() != null && Blockly.getMainWorkspace().topBlocks_.length != 0) {
                Blockly.getMainWorkspace().clear();
            }
           Blockly.Json.setWorkspace(Blockly.getMainWorkspace(), workspace);

            // Blockly sends an immediate change - we want to filter this out
            me.holdoffChanges = true;
            $timeout(function () {
                me.holdoffChanges = false;
            }, 500);
        };

        this.clearWorkspace = function () {
            if (Blockly.getMainWorkspace() != null && Blockly.getMainWorkspace().topBlocks_.length != 0) {
                Blockly.getMainWorkspace().clear();
            }
        };

        this.getWorkspace = function () {
            return Blockly.Json.getWorkspace(Blockly.getMainWorkspace());
        };

        this.setToolbox = function (options) {
            Blockly.updateToolbox(Blockly.parseToolboxTree_(options));
        };

        this.onChange = function (callback) {
            $(Blockly.mainWorkspace.getCanvas()).bind("blocklyWorkspaceChange", function () {
                if (me.holdoffChanges === false) {
                    // Send a notification
                    callback(Blockly.Json.getWorkspace(Blockly.getMainWorkspace()));
                }
            })
        };

        this.workspaceToPython = function(){
            return Blockly.Python.workspaceToCode();
        };
		
		this.workspaceToJavascript = function(){
            return Blockly.JavaScript.workspaceToCode();
        };

        this.workspaceToXMLstring = function(){
            return new XMLSerializer().serializeToString(Blockly.Xml.workspaceToDom(Blockly.getMainWorkspace()));
        };

        this.setModelAnswer = function (xml){
            if (Blockly.getMainWorkspace() != null && Blockly.getMainWorkspace().topBlocks_.length != 0) {
                Blockly.getMainWorkspace().clear();
            }

            var xmlObj = Blockly.Xml.textToDom(xml);
            Blockly.Xml.domToWorkspace(Blockly.getMainWorkspace(), xmlObj);
            
            me.holdoffChanges = true;
            $timeout(function () {
                me.holdoffChanges = false;
            }, 500);
        }
    }])
    .directive('ngBlockly', function ($window, $timeout, $rootScope, ngBlockly) {
        return {
            restrict: 'E',
            scope: { // Isolate scope
            },
            template: '<div style="height:100%" class="ng-blockly"></div>',
            link: function ($scope, element, attrs) {
                var options = ngBlockly.getOptions();
                Blockly.inject(element.children()[0], options);
            }
        };
    });